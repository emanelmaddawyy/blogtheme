<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html>
   <head>
      <title>Blog</title>
      <!-- Site Charset -->
      <meta charset="utf-8">
      <!-- Mobile Meta -->
      <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">
      <link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
      <link href="<?php echo get_template_directory_uri(); ?>/assets/css/fontawesome-all.min.css" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
      <!-- light owl -->

      <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/meanmenu.min.css">
      <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">

      <!-- BootStrap StyleSheets -->
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">

      <!-- Main StyleSheet -->
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
      <link href="<?php echo get_template_directory_uri(); ?>/assets/css/media.css" rel="stylesheet" type="text/css"/>

			<?php wp_head();?>
   </head>

 <body <?php body_class();?>>
<div id="page" class="site">
<div id="preloader">
          <div id="status">
          </div>
        </div>
       <header class="head">

        <div class="top-header">
         <div class="container">
           <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="#"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                  <ul class="navbar-nav">
                    <li class="nav-item ">
                      <a class="nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#about" >About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#experience">Experience</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#myWork">My Work</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </nav>
           </div>
         </div>
       </header>

	<?php

/*
 * If a regular post or page, and not the front page, show the featured image.
 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
 */
if ((is_single() || (is_page() && !twentyseventeen_is_frontpage())) && has_post_thumbnail(get_queried_object_id())):
    echo '<div class="single-featured-image-header">';
    echo get_the_post_thumbnail(get_queried_object_id(), 'twentyseventeen-featured-image');
    echo '</div><!-- .single-featured-image-header -->';
endif;
?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
