<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 * Template Name: footerTemplate
 */

?>

		</div><!-- #content -->

		<footer id="contact">
    <div class="container">
        <div class="seactionTitle text-center mb-60">
            <h1>Contact Us</h1>
            <div class="horizentalLine">
              <div class="top"></div>
              <div class="bottom"></div>
            </div>
          </div><!--sectonTitle-->
      <div class="row">
        <div class="col-md-8 d-none d-sm-none d-md-block">
          <div class="call">
            <h3>Send your inquiry</h3>
            <hr class="line">
          </div>
          <?php echo  do_shortcode('[contact-form-7 id="8" title="Contact form 1"]') ?>


        </div>

        <div class="col-md-4">
          <div class="call">
            <h3>Call us </h3>
            <hr class="line">
          </div>
          		
 
        <?php
            $args = array('pagename' => 'footer');
            $query = new WP_Query($args);

            $phone = "";
            $address = "";
            $mail = "";

            while($query -> have_posts()):
                $query -> the_post();

                $phone = types_render_field("phone");
                $address = types_render_field("address");
                $mail = types_render_field("mail");
                $facebook = types_render_field("face");
                $twitter = types_render_field("social_twitter");
                $whats = types_render_field("whats-up");
                $linked = types_render_field("linked-in");
              
            endwhile;
          ?>
       
	
          <div class="info">
            <ul class="list-unstyled">
        
              <li>
                <i class="fa fa-home" aria-hidden="true"></i>
                <span><?php echo $address; ?></span>
              </li>

              <li>
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span><?php echo $phone; ?></span>
              </li>

              <li>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span><?php echo $mail; ?></span>
              </li>
            </ul>
            </div>
					

          <div class="social">
            <a href="<?php echo $facebook; ?>"><i class="fab fa-facebook-square"></i> </a>
            <a href="<?php echo $twitter; ?>"><i class="fab fa-twitter"></i> </a>
            <a href="<?php echo $whats; ?>"><i class="fab fa-whatsapp"></i> </a>
            <a href="<?php echo $linked; ?>"><i class="fab fa-linkedin"></i> </a>
          </div>

          <div class="copyright">
            <p> All Rights Reserved © Dev. 2018 </p>
            <p> Design and develop  <a href="#"> Dubisign organization </a> </p>
          </div>

        </div>
        <div class=" d-block d-sm-block d-md-none">
          <div class="call">
            <h3>Send your inquiry</h3>
            <hr class="line">
          </div>
          <?php echo  do_shortcode('[contact-form-7 id="30" title="Contact form 1"]')?>
        </div>
      </div>
		</footer>

		<!-- JavaScript -->
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/popper.min.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/fontawesome-all.min.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/select2.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.js" type="text/javascript"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.meanmenu.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup.js"></script>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>

		<?php wp_footer();?>
   </body>
</html>
