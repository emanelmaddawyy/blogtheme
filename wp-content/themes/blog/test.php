<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 * Template Name: testTemplate
 */
get_header();?>
  
 
<div class="info">
<div class="container">
  <div class="row">
    <div class="col-md static">
      <div class="personInfo ">
                     
      <h5 class="mb-10">HELLO, MY NAME IS</h5>
          <?php
            $args = array('pagename' => 'test');
            $query = new WP_Query($args);

            while($query -> have_posts()):
                $query -> the_post();

                $name = types_render_field("your-name");
                $description = types_render_field("your-description");
                $your_CV = types_render_field("your-cv");
                $your_image = types_render_field("your-image", array("raw" => true));
            endwhile;
          ?>
               <h1 class="mb-10"><?php echo $name; ?></h1>
               <p class="font-16 line-height-28"> <?php echo $description; ?> </p>

                <div class="buttonInfo mt-30">
                  <a class="btn lg-btn" href="<?php echo $your_CV; ?>">My Work</a>
                </div>

               </div><!--personInfo-->
             </div><!--col static-->
             
            <div class="col-md text-right">
							<div class="wow fadeInRight animated" data-wow-duration="2s" data-wow-delay=".1s"  style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInLeft;">
              <img src="<?php echo $your_image; ?>">
              </div> <!--wow-->
            </div><!--col-->
  </div><!--row-->
</div><!--container-->
</div><!--info-->


      
 <?php 
 get_footer();