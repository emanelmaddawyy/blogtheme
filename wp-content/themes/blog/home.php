<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 * Template Name: HomeTemplate
 */

get_header();?>

         <div class="info">
         <div class="container">
           <div class="row">
             <div class="col-md static">
               <div class="personInfo ">
								<h5 class="mb-10">HELLO, MY NAME IS</h5>
								
								<?php $pages = get_pages(array(
						'meta_key' => '_wp_page_template',
						'meta_value' => 'home.php'
							));
						foreach($pages as $page) :
						?>
						<?php if( get_field('person_name', $page->ID) ): ?>
						<h1 class="mb-10"><?php echo get_field('person_name', $page->ID) ?></h1>
						<p class="font-16 line-height-28"><?php echo get_field('person_description', $page->ID) ?></p>
						<?php endif; ?>
						<?php endforeach; ?>

                <div class="buttonInfo mt-30">
                  <a class="btn lg-btn" href="#myWork">My Work</a>
                </div>
               </div><!--personInfo-->
             </div><!--col static-->

             <div class="col-md text-right">
								<div class="wow fadeInRight animated" data-wow-duration="2s" data-wow-delay=".1s"  style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInLeft;">
								<?php $pages = get_pages(array(
						'meta_key' => '_wp_page_template',
						'meta_value' => 'home.php'
							));
						foreach($pages as $page) :
						?>
						<?php if( get_field('person_name', $page->ID) ): ?>
						<img src="<?php echo get_field('person_image'); ?>">
						<?php endif; ?>
						<?php endforeach; ?>
               </div> <!--wow-->
             </div><!--col-->
           </div><!--row-->
         </div><!--container-->
       </div><!--info-->
       <div id="about">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
           <div class="seactionTitle text-center mb-60">
             <h1>about me</h1>
             <div class="horizentalLine">
               <div class="top"></div>
               <div class="bottom"></div>
             </div>
           </div><!--sectonTitle-->
           </div> <!--col-->
           <div class="col-md about">
            <div class="wow fadeInRight animated" data-wow-duration="2s" data-wow-delay=".1s"  style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInLeft;">
             <div class="personInfo left">
							
						 <?php $pages = get_pages(array(
						'meta_key' => '_wp_page_template',
						'meta_value' => 'home.php'
							));
						foreach($pages as $page) :
						?>
						<?php if( get_field('about_me', $page->ID) ): ?>
						<?php echo get_field('about_me', $page->ID) ?>

						<div class="buttonInfo mt-30">
                    <a class="btn lg-btn" target="_blank" href="<?php echo get_field('cv_url', $page->ID) ?>">Download my CV</a>
                  </div>
						<?php endif; ?>
						<?php endforeach; ?>
             </div>
           </div>
					 </div>


					 <?php if( have_rows('skills') ): ?>
					 <div class="col-md">
            <div class="wow fadeInRight animated" data-wow-duration="2s" data-wow-delay=".1s"  style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInRight;">
					<?php while( have_rows('skills') ): the_row();

					// vars
					$skillName = get_sub_field('skill_name');
					$skillProgress = get_sub_field('skill_progress');
					?>



					<div class="barWrapper">
							<span class="progressText"><B><?php echo $skillName; ?></B></span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $skillProgress; ?>" aria-valuemin="0" aria-valuemax="100" >
							</div>
						</div>
					</div>

					<?php endwhile; ?>
					<?php endif; ?>
    </div><!--row-->
  </div><!--container-->
  </div>
  </div>
</div>
<section class="experience" id="experience">
   <div class="info">
    <div class="container">
      <div class="row">

        <div class="col-md-12">
            <div class="seactionTitle text-center mb-60">
                <h1>MY EXPERIENCE</h1>
                <div class="horizentalLine">
                  <div class="top"></div>
                  <div class="bottom"></div>
                </div>
            </div><!--sectonTitle-->
        </div>


      <?php if( have_rows('experience') ): ?>
      <?php while( have_rows('experience') ): the_row(); 
      $title =  get_sub_field('title');
      $icons =   get_sub_field('icons');
      $item =    get_sub_field('item');
      ?>
        <div class="col-md-6">
            <ul class="list-unstyled">
                <div class="wow fadeInRight animated" data-wow-duration="2s" data-wow-delay=".1s"  style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name:pulse;">
                <li class="media">
                <a><?php echo $icons ?> </a>
                  <div class="media-body">
                    <h5 class="mt-0 mb-1"><?php echo $title ?></h5>
                    <p><?php echo $item ?> </p>
                  </div>
                </li>
              </div>
            </ul>
        </div>
        <?php endwhile; ?>
      <?php endif; ?>
 
      </div>
    </div>
  </div>
</section>

  <section class="myWork" id="myWork">
      <div class="seactionTitle text-center mb-60">
          <h1>My Work</h1>
          <div class="horizentalLine">
            <div class="top"></div>
            <div class="bottom"></div>
          </div>
        </div><!--sectonTitle-->
      <div id="Container">
          <div class="container">
            <div class="buttons">
              
            <button type="button" class=" btn filter"  data-filter="all">All Works</button>
            
             <?php if( have_rows('work_categories') ): ?>
              <?php while( have_rows('work_categories') ): the_row(); 

              $workCategoryName = get_sub_field('work_category_name');
              ?>

              <button type="button" class=" btn filter"  data-filter=".<?php echo $workCategoryName; ?>>"><?php echo $workCategoryName; ?></button>

                <?php endwhile; ?>
              <?php endif; ?>

            </div>


  

              <?php
              $i = 0;
              if( have_rows('work_categories') ): ?>
                <?php while( have_rows('work_categories') ): the_row(); ?>
                    <?php $workCategoryName = get_sub_field('work_category_name');

                    while( have_rows('work_items') ): the_row(); ?>
                    <?php
                    if (($i % 4) == 0): ?>
                    <div class="row">
                    <?php
                    endif;
                    ?>

                      <?php $workItemImage = get_sub_field('work_item_image');
                      $workItemUrl = get_sub_field('work_item_url'); ?>

                      <div class="col-md col-sm-6 mix <?php echo $workCategoryName; ?>">
                      <div class="new">
                        <img class="img-fluid" src="<?php echo $workItemImage; ?>">
                        <div class="link"><a target="_blank" href="<?php echo $workItemUrl ?>"><i class="fas fa-link"></i></a></div>
                      </div>
                      </div>
                    
                      <?php $i++; ?>

                    <?php
                    if (($i % 4) == 0): ?>
                    </div>
                    <?php
                    endif;
                    ?>

                    <?php endwhile; ?>

                <?php endwhile; ?>
              <?php endif; ?>

   
            </div><!--row-->
          </div><!--container-->
        </div><!--containerID-->
  </section>

<?php get_footer();