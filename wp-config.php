<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blogTheme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J4!j5iec+ZYcZ]D=`K%6?:GE*_ZCHsaODZEb599(nelcSe#;zX3!ys4(*6C-`BE.');
define('SECURE_AUTH_KEY',  'S-8|=YcOlkh-(-;j6YLI:Pz9:i=p{]Q5`,Q:Ofr@L%2,cn<NKyk/y%Dxt_ bX?]{');
define('LOGGED_IN_KEY',    '<.-8T;%t6C>lA$bkQMuEV2nR3P?V}UnQ4N0t#j0Y{dkO,-eQh:56lsQgJX_r2,+>');
define('NONCE_KEY',        'UcN00rT.?/f?$]bb` d5fPV`G%I3cl#_6%VdqIG$gxdriLl2}fnSbBi~MD=g$i)Z');
define('AUTH_SALT',        'PV3;zVwFXy&83}rO)c;vVUzE=@UG4d|b.IL<T*tHxcqW?~SeSG:s`k=FN3*v=Kv0');
define('SECURE_AUTH_SALT', 'q,xFSt[]OzURRLx4=EkM(=KO6{vM]X$$FM*bDF9U2*T+D_/bW&6r](3tAY&%wL$S');
define('LOGGED_IN_SALT',   'da Z;D;CWWPF6h]t~* {dAE40Oq$0~r~,T:SD[6]f^l)#uUr08u_e/A+nL%4utl)');
define('NONCE_SALT',       '[g+b`X SF/[hh2~.g,K$MTr{GSvD-#lsa# Mg@U6|V pJYN viC&w2516~YmWscm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
